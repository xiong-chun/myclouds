### 温馨提示
该项目已停止维护，推荐关注升级换代版本。
- [盘古开发框架](https://gitee.com/pulanos/pangu-framework)

### MyClouds截屏
![输入图片说明](https://images.gitee.com/uploads/images/2020/0418/151811_64047579_431745.jpeg "MyClouds企业级微服务开源平台-资源模块.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0418/151853_b43471fb_431745.jpeg "MyClouds企业级微服务开源平台v2.1.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0418/151904_c10c726c_431745.png "MyClouds开发框架-用户管理.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0418/151919_6d52d04c_431745.png "MyClouds开发框架-角色管理.png")